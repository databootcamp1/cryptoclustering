# CryptoClustering Challenge
Creator: Melissa Acevedo

## Overview
This analysis aims to cluster cryptocurrencies based on their market data using K-means clustering. Two approaches will be explored - one using the original scaled data and another using Principal Component Analysis (PCA) for feature reduction. The following steps will be performed:

1. **Data Preparation**

- Rename the Jupyter notebook file to Crypto_Clustering.ipynb.
- Load the crypto market data from crypto_market_data.csv into a DataFrame.
- Obtain summary statistics and visualize the data.

2. **Prepare the Data**

- Normalize the data using the StandardScaler() module.
- Create a DataFrame with the scaled data and set the "coin_id" as the index.

3. **Find the Best Value for k Using Original Scaled Data**

- Utilize the elbow method to identify the optimal value for k.
- Create a line chart with inertia values for different k values.

4. **Cluster Cryptocurrencies with K-means Using Original Scaled Data**

- Initialize the K-means model with the best value for k.
- Fit and predict clusters using the original scaled DataFrame.
- Visualize the clusters using a scatter plot.

5. **Optimize Clusters with Principal Component Analysis (PCA)**

- Perform PCA on the original scaled DataFrame to reduce features to three principal components.
- Retrieve explained variance to determine the total information attributed to each component.

6. **Find the Best Value for k Using PCA Data**

- Apply the elbow method on the PCA data to find the optimal k value.
- Create a line chart with inertia values for different k values.

7. **Cluster Cryptocurrencies with K-means Using PCA Data**

- Initialize the K-means model with the best value for k using PCA data.
- Fit and predict clusters using the PCA DataFrame.
- Visualize the clusters using a scatter plot.

8. **Analysis and Conclusion**

- Compare the results obtained from clustering with original scaled data and PCA.
- Evaluate the impact of using fewer features for clustering.

## Questions to Answer
1. What is the best value for k when using the original scaled data?
2. What is the total explained variance of the three principal components obtained from PCA?
3. What is the best value for k when using the PCA data?
4. Does the best k value differ when using the original data compared to PCA?
5. What is the impact of using fewer features to cluster the data using K-Means?



